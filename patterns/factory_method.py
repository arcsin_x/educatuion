#!/usr/bin/python3.6

from abc import ABC, abstractmethod

class FurnitureFabric(ABC):
    # Абстрактный класс
    @abstractmethod
    def create_furniture(self):
        # Абстрактный фабричный метод
        pass

    def operation(self) -> str:
        # Вызываем фабричный метод, чтобы получить объект-продукт
        product = self.create_furniture()
        # Работаем с этим продуктом
        result = f"Фабрика мебели: Фабричный код работает с типом {product.operation()}"
        return result

#Конкретные фабрики перегружает метод абстрактного класса
class TableFactory( ):
    def create_furniture(self):
        return table()

class SeatsFactory(FurnitureFabric):
    def create_furniture(self):
        return seat()


class Furniture(ABC):
    """
    Интерфейс мебели
    Здесь находятся все операции, которые должны выполнять объекты мебели
    """
    @abstractmethod
    def operation(self) -> str:
        pass

class Table(Furniture):
    def operation(self):
        return "{Результат стола}"


class Seat(Furniture):
    def operation(self):
        return "{Результат стула}"


def client_code(creator: FurnitureFabric) -> None:
    print(f"Клиент: не в курсе о типе создаваемого объекта, но начинает работать.\n"
          f"{creator.operation()}", end="")

if __name__ == "__main__":
    print("Приложение: в клиентский код передан 'стол'.")
    client_code(Table())
    print("\n")

    print("Приложение: в клиентский код передан 'стул'.")
    client_code(Seat())